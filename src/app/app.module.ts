import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { MoviesPage } from '../pages/movies/movies';
import { ToSeePage } from '../pages/toSee/toSee';
import { FavouritesPage } from '../pages/favourites/favourites';
import { UpcomingPage } from '../pages/upcoming/upcoming';
import { TabsPage } from '../pages/tabs/tabs';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { MoviesProvider } from '../providers/movies/movies';
import { UserProvider } from '../providers/user/user';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';


import { FIREBASE_CONFIG } from './app.firebase.config';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MoviesPage,
    ToSeePage,
    FavouritesPage,
    UpcomingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
        scrollPadding: false,
        scrollAssist: false,
        autoFocusAssist: false
    }),
    HttpModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MoviesPage,
    ToSeePage,
    FavouritesPage,
    UpcomingPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MoviesProvider,
    UserProvider
  ]
})
export class AppModule {}
