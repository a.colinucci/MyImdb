import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';


@Injectable()
export class UserProvider {

  private favoriteMovies: any;
  private toSeeMovies: any;
  private profile: any;

  constructor(public http: Http,
              private afAuth:AngularFireAuth,
              private afDatabase: AngularFireDatabase) {

      if (this.afAuth.auth.currentUser != null) {
        this.favoriteMovies = this.afDatabase.list(`profile/${this.afAuth.auth.currentUser.uid}/favourite-movies`);
        this.toSeeMovies = this.afDatabase.list(`profile/${this.afAuth.auth.currentUser.uid}/to-see-movies`);
        this.profile = this.afDatabase.list(`profile/${this.afAuth.auth.currentUser.uid}/Profile`);
      }

  }

  getFavourites() {
    return this.favoriteMovies;
  }

  addToFavourite(movie: any) {
    return this.favoriteMovies.push(movie);
  }

  removeFromFavourites(movie: any) {
    return this.favoriteMovies.remove(movie.key);
  }

  getToSee() {
    return this.toSeeMovies;
  }

  addToToSee(movie: any) {
    return this.toSeeMovies.push(movie);
  }

  removeFromToSee(movie: any) {
    return this.toSeeMovies.remove(movie.key);
  }

  addProfile(profile: any) {
    this.afAuth.authState.take(1).subscribe(auth => {
      this.afDatabase.list(`profile/${auth.uid}/Profile`).push(profile);
    });
  }

  getProfile() {
    return this.getProfile();
  }

}
