import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MoviesProvider {

  private apiKey: string = '?api_key=b1887adc1b5fdc477d15d2b46f2d3809';
  private language: string = '&language=it-IT';
  private url: string = 'https://api.themoviedb.org/3/';
  private trend:string;

  constructor(public http: Http) {

  }

  getPopularMovies(page:number=1) {
    this.trend = 'popular';
    return this.http.get(this.url + 'movie/' + this.trend + this.apiKey + this.language + '&page=' + page).map(res => res.json());
  }

  getUpcomingMovies(page:number=1) {
    this.trend = 'upcoming';
    return this.http.get(this.url + 'movie/' + this.trend + this.apiKey + this.language + '&page=' + page).map(res => res.json());
  }

  getMovieById(id:number) {
    return this.http.get(this.url + 'movie/' + id + this.apiKey + this.language).map(res => res.json());
  }

  getSimilarMovies(id:number, page:number=1) {
    this.trend = 'similar';
    return this.http.get(this.url + 'movie/' + id + "/" + this.trend + this.apiKey + this.language + '&page=' + page).map(res => res.json());
  }

  getMovieTrailer(id:number) {
    return this.http.get(this.url + 'movie/' + id + "/videos" + this.apiKey + this.language).map(res => res.json());
  }

  searchMovies(key: string) {
    return this.http.get(this.url + 'search/movie' + this.apiKey + this.language + "&query=" + key + "&page=1&include_adult=false")
               .map(res => res.json());
  }

  getMoviesByGenre(genre: number) {
    return this.http.get(this.url + 'genre/' + genre + '/movies' + this.apiKey + this.language + "&page=1&include_adult=false")
               .map(res => res.json());
  }

}
