import { Component, ViewChildren, QueryList  } from '@angular/core';
import { Platform, NavController, ActionSheetController, Slides, LoadingController, ToastController } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-movies',
  templateUrl: 'movies.html'
})

export class MoviesPage {

  private loading: any;
  private isOn: boolean = false;
  private hidden: boolean;
  private filteredMovies: any = [];
  private crimeMovies: any = [];
  private comedyMovies: any = [];
  private animationMovies: any = [];
  private popularMovies: any = [];
  private westernMovies: any = [];
  private imageUrl:string;
  private page:number;

  @ViewChildren(Slides) slides: QueryList<Slides>;

  constructor(public navCtrl: NavController,
              private moviesProvider:MoviesProvider,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController,
              private afAuth:AngularFireAuth,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController) {

    this.imageUrl = 'https://image.tmdb.org/t/p/w500/';
    this.page = 1;
    this.hidden = true;
    this.loading = this.loadingCtrl.create({
      content: `Sto caricando i film`
    });
  }

  ngAfterViewInit() {

    this.presentLoadingDefault();

    let slides = this.slides.toArray();

    for (let slide of slides) {
      slide.slidesPerView = 3;
      slide.spaceBetween = '2px';
      slide.centeredSlides = true;
      slide.zoom = true;
      slide.loop = true;
    }

  }

  ionViewWillEnter() {
    this.page = 1;
    this.initialize();
  }

  ionViewDidLoad() {
    this.loading.dismiss();
    this.hidden = false;
  }

  showTitle(title: string) {
    this.presentToast(title);
  }

  openMovieDetails(movieId) {

    this.moviesProvider.getMovieById(movieId).subscribe(
      movie => {
        this.navCtrl.push('MovieDetailsPage', {
          movie: movie
        });

    });
  }

  logoutUser(): void {
    
    this.afAuth.auth.signOut().then((data: any) => {

      window.localStorage.clear();
      if (window.location.reload()) {
        this.navCtrl.setRoot('LoginPage');
      };

    }).catch((error: any) => {
      console.dir(error);
    });

  }

  searchMovies(key: any) {

    if (key.target.value && key.target.value.trim() != '') {

      this.moviesProvider.searchMovies(key.target.value).subscribe(
        movies => {
          this.filteredMovies = movies.results;
        }
      );
    }

 }

 setState(): void {
   this.isOn = !this.isOn;
 }

toggleDetails() {
   this.hidden = !this.hidden;
   this.isOn = !this.isOn;
 }

 private presentLoadingDefault() {
   this.loading.present();
 }

 private initialize() {

   this.moviesProvider.getPopularMovies(this.page).subscribe(
     movies => {
       this.popularMovies = movies.results;
     });

   this.moviesProvider.getMoviesByGenre(37).subscribe(
     movies => {
       this.westernMovies = movies.results;
     });

   this.moviesProvider.getMoviesByGenre(80).subscribe(
     movies => {
       this.crimeMovies = movies.results;
     });

   this.moviesProvider.getMoviesByGenre(16).subscribe(
     movies => {
       this.animationMovies = movies.results;
     });

   this.moviesProvider.getMoviesByGenre(35).subscribe(
     movies => {
       this.comedyMovies = movies.results;
     });

 }

 private presentToast(message: string) {

   let toast = this.toastCtrl.create({
     message: message,
     duration: 3000,
     position: 'bottom'
   });

   toast.present();
 }

}
