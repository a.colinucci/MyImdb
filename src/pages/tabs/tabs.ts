import { Component } from '@angular/core';
import { MoviesPage } from '../movies/movies';
import { FavouritesPage } from '../favourites/favourites';
import { ToSeePage } from '../toSee/toSee';
import { UpcomingPage } from '../upcoming/upcoming';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MoviesPage;
  tab2Root = FavouritesPage;
  tab3Root = ToSeePage;
  tab4Root = UpcomingPage;

  constructor() {

  }
  
}
