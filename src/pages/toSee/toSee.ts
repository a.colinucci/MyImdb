import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Platform, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { MoviesProvider } from '../../providers/movies/movies'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-favourites',
  templateUrl: 'toSee.html',
})
export class ToSeePage {

  private toSeeMovies$: Observable<any[]>;
  private imageUrl: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController,
              private userProvider:UserProvider,
              private moviesProvider:MoviesProvider,
              private toastCtrl: ToastController) {

    this.imageUrl = 'https://image.tmdb.org/t/p/w500/';
  }

  ionViewWillEnter() {

    this.toSeeMovies$ = this.userProvider.getToSee() //DB list
                                             .snapshotChanges() //Key and value
                                             .map( changes => {
                                               return changes.map(c => ({
                                                 key: c.payload.key, ...c.payload.val(),
                                               }))
                                             })

  }

  showTitle(title: string) {
    this.presentToast(title);
  }

  deleteFromToSee(movie: any) {
    this.userProvider.removeFromToSee(movie);
    this.presentToast(movie.title + " eliminato dalla tua lista.");
  }

  openMovieDetails(movieId) {
    this.moviesProvider.getMovieById(movieId).subscribe(
      movie => {
        this.navCtrl.push('MovieDetailsPage', {
          movie: movie
        });

    });
  }

  private presentToast(message: string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

}
