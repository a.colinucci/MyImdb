import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform, ToastController } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';

@Component({
  selector: 'page-upcoming',
  templateUrl: 'upcoming.html'
})
export class UpcomingPage {

  private upcomingMovies:any = [];
  private page:number;
  private imageUrl:string;

  constructor(public navCtrl: NavController,
              private moviesProvider: MoviesProvider,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController,
              private toastCtrl: ToastController) {

     this.imageUrl = 'https://image.tmdb.org/t/p/w500/';
  }

  ionViewWillEnter() {
    this.page = 1;

    this.moviesProvider.getUpcomingMovies(this.page).subscribe(
      movies => {
        this.upcomingMovies = movies.results;
    });
  }

  doInfinite(infiniteScroll) {
     this.page += 1;

     this.moviesProvider.getUpcomingMovies(this.page).subscribe(
       movies => {
         infiniteScroll.complete();
         this.upcomingMovies.push.apply(this.upcomingMovies, movies.results);
     });
  }

  showTitle(title: string) {
    this.presentToast(title);
  }

  openMovieDetails(movieId) {
    this.moviesProvider.getMovieById(movieId).subscribe(
      movie => {
        this.navCtrl.push('MovieDetailsPage', {
          movie: movie
        });
    });
  }

  private presentToast(message: string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

}
