import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';
import { UserProvider } from '../../providers/user/user';
import { Platform, ActionSheetController  } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-movie-details',
  templateUrl: 'movie-details.html',
})

export class MovieDetailsPage {

  private trailerDisponibile:boolean;
  private playTrailer:boolean;
  private loading:boolean;
  private videoUrl:any; //Sanitized Url
  private imageUrl:string;
  private movie:any;
  private similarMovies:any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private moviesProvider:MoviesProvider,
              private userProvider:UserProvider,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController,
              public sanitizer:DomSanitizer,
              private toastCtrl: ToastController
            ) {

    this.movie = this.navParams.get('movie');
    this.imageUrl = 'https://image.tmdb.org/t/p/w500/';
    this.loading = true;
    this.playTrailer = false;
    this.trailerDisponibile = false;

  }

  ionViewDidLoad() {

    this.moviesProvider.getMovieTrailer(this.movie.id).subscribe(
      trailer => {
        if (trailer.results[0] != undefined) {
          this.trailerDisponibile = true;
          this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + trailer.results[0].key);
        }
    });

  }

  ionViewWillEnter() {
    //Get similar movies [just page 1]
    this.moviesProvider.getSimilarMovies(this.movie.id, 1).subscribe(
      movies => {
        this.similarMovies = movies.results
    });

  }

  showTitle(title: string): void {
    this.presentToast(title);
  }

  openMovieDetails(movieId) {
    this.moviesProvider.getMovieById(movieId).subscribe(
      movie => {
        this.navCtrl.push('MovieDetailsPage', {
          movie: movie
        });
    });
  }

  hasLoaded(): void {
    this.loading = false;
  }

  play(): void {
    this.playTrailer = true;
    this.presentToast('Trailer di " ' + this.movie.title + ' " in avvio');
  }

  addListFavourites(): void {

    //Controllare se è già inserito
    this.userProvider.addToFavourite(this.movie);
    this.presentToast(this.movie.title + " è stato aggiunto ai preferiti.");

  }

  addListToSee(): void {

    //Controllare se è già inserito
    this.userProvider.addToToSee(this.movie);
    this.presentToast(this.movie.title + " è stato aggiunto alla tua lista.");

  }

  private presentToast(message: string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

}
