import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { TabsPage } from '../tabs/tabs';
import { Platform } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { User } from "../../models/user";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;

  constructor(public platform: Platform,
              public keyboard: Keyboard,
              public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private aFauth: AngularFireAuth) {

      if((window.localStorage.getItem('username') === "undefined" || window.localStorage.getItem('username') === null) &&
        (window.localStorage.getItem('password') === "undefined" || window.localStorage.getItem('password') === null)) {

      } else {
        this.navCtrl.setRoot(TabsPage);
      }
  }

  async login(user: User) {
    try {
      const result = await this.aFauth.auth.signInWithEmailAndPassword(user.email, user.password);
      if (result) {

        if (this.aFauth.auth.currentUser.emailVerified) {

          window.localStorage.setItem('username', user.email);
          window.localStorage.setItem('password', user.password);

          this.navCtrl.setRoot(TabsPage);
          this.presentToast('Benvenuto in "The Movie Database"');

        } else {
          this.presentToast("L' account non è stato ancora verificato.");
        }
      }
    } catch (e) {
      this.presentToast("Credenziali di accesso non valide.");
    }
  }

  register():void {
    this.navCtrl.push('RegisterPage');
  }

  private presentToast(message: string):void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
