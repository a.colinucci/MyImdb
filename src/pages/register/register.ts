import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';

import { User } from "../../models/user";
import { Profile } from "../../models/profile";


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  profile = {} as Profile;
  user = {} as User;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private aFauth: AngularFireAuth,
              private userProvider:UserProvider) {

  }

  async register(user: User) {

    try {
      const result = await this.aFauth.auth.createUserWithEmailAndPassword(user.email, user.password);

      if (result) {

        this.aFauth.auth.currentUser.sendEmailVerification().then((result) => {

          this.presentToast("Abbiamo inviato una mail di conferma al tuo indirizzo. Verifica il tuo account prima di proseguire.");
          this.navCtrl.pop();

          this.userProvider.addProfile(this.profile);

        }, (error) => {
          this.presentToast(error);
        });

      }

    } catch(e) {
      this.presentToast(e);
    }

  }

  presentToast(message: string):void {

    const toast = this.toastCtrl.create({
      message: message,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
    
  }

}
